module chess

require (
	github.com/gin-contrib/multitemplate v0.0.0-20190301062633-f9896279eead
	github.com/gin-gonic/gin v1.3.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/stretchr/testify v1.2.2
	gopkg.in/go-playground/assert.v1 v1.2.1
)
