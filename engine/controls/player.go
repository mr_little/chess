package controls

import (
	"github.com/gofrs/uuid"
	"math/rand"
)

var r *rand.Rand

func init() {
	r = rand.New(rand.NewSource(10))
}

func getColor(min, max int, r *rand.Rand) string {
	n := r.Intn(max-min+1) + min
	if n == 1 {
		return "black"
	}
	return "white"
}

type Player struct {
	Color string
	Id    string
}

func NewPlayer(color string) *Player {

	id, _ := uuid.NewV4()
	if color == "" {
		color = getColor(0, 1, r)
	}

	return &Player{Color: color, Id: id.String()}
}
