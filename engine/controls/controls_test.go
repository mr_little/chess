package controls

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPositionToCode(t *testing.T) {
	move := NewMove(NewPosition(0, 0), NewPosition(0,1))

	assert.Equal(t, "a1", PositionToCode(move.From))
	assert.Equal(t, "b1", PositionToCode(move.To))
	assert.Equal(t, "a1b1", move.String())
}

func TestCodeToMoveOk(t *testing.T) {
	res, err := CodeToMove("e2e4")
	assert.NoError(t, err)
	assert.Equal(t, "1 4", res.From.String())
	assert.Equal(t, "3 4", res.To.String())
}

func TestCodeToMoveNotValidFirstLetter(t *testing.T) {
	_, err := CodeToMove("m2e4")
	assert.Error(t, err)
	assert.EqualError(t, err, "code must contains 4 digits")
}

func TestCodeToMoveNotValidSecondLetter(t *testing.T) {
	_, err := CodeToMove("e2m4")
	assert.Error(t, err)
	assert.EqualError(t, err, "code must contains 4 digits")
}

func TestCodeToMoveNotValidLength(t *testing.T) {
	_, err := CodeToMove("e2e9a3")
	assert.Error(t, err)
	assert.EqualError(t, err, "code must contains 4 digits")
}

func TestCodeToMoveNotValidDigit(t *testing.T) {
	_, err := CodeToMove("e2e9")
	assert.Error(t, err)
	assert.EqualError(t, err, "from and to must be between 1 and 8")
}