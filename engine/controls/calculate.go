package controls

import (
	"fmt"
	"strconv"
	"strings"
)

func PositionToCode(p Position) string {
	a := map[int]string{
		0: "a",
		1: "b",
		2: "c",
		3: "d",
		4: "e",
		5: "f",
		6: "g",
		7: "h",
	}
	return fmt.Sprintf("%s%d", a[p.Col], p.Row+1)
}

func CodeToMove(fromToCode string) (move *Move, err error) {
	a := map[string]int{
		"a": 0,
		"b": 1,
		"c": 2,
		"d": 3,
		"e": 4,
		"f": 5,
		"g": 6,
		"h": 7,
	}

	code := strings.Split(fromToCode, "")
	if len(code) < 4 || len(code) > 4 {
		return nil, fmt.Errorf("code must contains 4 digits")
	}

	if _, ok := a[code[0]]; !ok {
		return nil, fmt.Errorf("code must contains 4 digits")
	}

	if _, ok := a[code[2]]; !ok {
		return nil, fmt.Errorf("code must contains 4 digits")
	}

	from, err := strconv.Atoi(code[1])
	if err != nil {
		return nil, err
	}

	to, err := strconv.Atoi(code[3])
	if err != nil {
		return nil, err
	}

	if (from < 1 || from > 8) || (to < 1 || to > 8) {
		return nil, fmt.Errorf("from and to must be between 1 and 8")
	}

	p1 := NewPosition(from-1, a[code[0]])
	p2 := NewPosition(to-1, a[code[2]])

	move = NewMove(p1, p2)

	return
}
