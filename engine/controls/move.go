package controls

import (
	"fmt"
)

type Move struct {
	From Position `json:"from"`
	To   Position `json:"to"`
}

func NewMove(from, to Position) *Move {
	return &Move{From: from, To: to}
}

func NewMoveFromString(s string) (m *Move, err error) {
	m, err = CodeToMove(s)
	return
}

func (m Move) String() string {
	return fmt.Sprintf("%s%s", PositionToCode(m.From), PositionToCode(m.To))
}
