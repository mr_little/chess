package controls

import "fmt"

type Positioned interface {
	GetRow() int
	SetRow(int)
	GetCol() int
	SetCol(int)
}

type Position struct {
	Row int `json:"raw"`
	Col int `json:"call"`
}

func (p Position) GetRow() int {
	return p.Row
}

func (p Position) GetCol() int {
	return p.Col
}

func (p *Position) SetRow(r int) {
	p.Row = r
}

func (p *Position) SetCol(c int) {
	p.Col = c
}

func (p Position) String() string {
	return fmt.Sprintf("%d %d", p.Row, p.Col)
}

func NewPosition(row, col int) Position {
	return Position{Row: row, Col: col}
}
