package pieces

import (
	"chess/engine/controls"
)

const (
	PAWN = iota
	ROOK
	BISHOP
	KNIGHT
	QUEEN
	KING
)

type CheckmatePiece interface {
	Move(moveTo controls.Position) (err error)
	PossibleMoves(b *Board) (moves []controls.Move, err error)
	GetName() string
	GetPosition() controls.Position
	GetColor() string
	GetType() int
}

type Piece struct {
	Name            string
	Color           string
	CurrentPosition controls.Position
	IsEaten         bool
	Type            int
}

func (p Piece) GetName() string {
	return p.Name
}

func (p Piece) GetPosition() controls.Position {
	return p.CurrentPosition
}

func (p Piece) GetColor() string {
	return p.Color
}

func (p Piece) GetType() int {
	return p.Type
}

func (p *Piece) Move(moveTo controls.Position) (err error) {
	p.CurrentPosition.Row = moveTo.GetRow()
	p.CurrentPosition.Col = moveTo.GetCol()
	return
}
