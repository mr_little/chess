package pieces

import (
	"chess/engine/controls"
)

func NewQueen(row, col int, color string) *Queen {
	q := Queen{}
	q.Color = color
	q.CurrentPosition.Row = row
	q.CurrentPosition.Col = col
	q.Name = "q"
	q.Type = QUEEN
	return &q
}

type Queen struct {
	Piece
}

func (q Queen) PossibleMoves(board *Board) (moves []controls.Move, err error) {

	m, err := board.CalculateDiagonal(&q, -1)
	if err != nil {
		return
	}
	for i := range m {
		moves = append(moves, m[i])
	}

	m, err = board.CalculateStraight(&q, -1)
	if err != nil {
		return
	}
	for i := range m {
		moves = append(moves, m[i])
	}

	return
}
