package pieces

import (
	"chess/engine/controls"
	"fmt"
	"sync"
)

const (
	LessThanZero       = "X and Y cannot been less than zero"
	XGreaterThanCeilLen = "X cannot been greater than ceil len"
	YGreaterThanCeilLen = "Y cannot been greater than ceil len"
)

type Ceil struct {
	IsOccupied bool
	Item       CheckmatePiece
}

func (c Ceil) String() string {
	if c.Item != nil {
		return c.Item.GetName()
	}
	return " "
}

func NewBoard() *Board {
	return &Board{}
}

type Board struct {
	BlackOnTheTop bool
	CeilList      [8][8]Ceil
}

func (b Board) GetCeil(p controls.Position) (ceil *Ceil, err error) {

	//Check X
	if p.Row < 0 || p.Col < 0 {
		return nil, fmt.Errorf(LessThanZero)
	} else if len(b.CeilList)-1 < p.Row {
		return nil, fmt.Errorf(XGreaterThanCeilLen)
	} else if len(b.CeilList[p.Row])-1 < p.Col {
		return nil, fmt.Errorf(YGreaterThanCeilLen)
	}

	ceil = &b.CeilList[p.Row][p.Col]
	return
}

func (b *Board) UpdateCeil(isOccupied bool, item CheckmatePiece, p controls.Position) (err error) {
	_, err = b.GetCeil(p)
	if err != nil {
		return
	}
	b.CeilList[p.Row][p.Col].IsOccupied = isOccupied

	b.CeilList[p.Row][p.Col].Item = item

	return
}

func (b *Board) addBoardItem(row, col int, item CheckmatePiece) {
	b.CeilList[row][col].Item = item
	b.CeilList[row][col].IsOccupied = true

}

func (b *Board) Init(blackOnTheTop bool) {

	b.BlackOnTheTop = blackOnTheTop

	items := getBoardItems(blackOnTheTop, getAllPieces())

	for i, item := range items {
		b.addBoardItem(item.GetPosition().Row, item.GetPosition().Col, items[i])
	}
}

func (b Board) Draw() string {

	rows := [8]string{}

	for i := 0; i < 64; i++ {
		col := i % 8
		row := i / 8
		rows[row] += " " + b.CeilList[row][col].String() + " |"
	}

	str := "   +---+---+---+---+---+---+---+---+\n"
	for i := 7; i >= 0; i-- {
		str += fmt.Sprint(" ", i+1, " |", rows[i])
		str += "\n   +---+---+---+---+---+---+---+---+\n"
	}
	str += "     A   B   C   D   E   F   G   H\n"
	return str
}

func (b Board) GetPiecesForColor(color string) []CheckmatePiece {
	var result []CheckmatePiece

	for i := 0; i <= 63; i++ {

		ceil := i % 8
		row := i / 8

		if b.CeilList[row][ceil].IsOccupied && b.CeilList[row][ceil].Item.GetColor() == color {
			result = append(result, b.CeilList[row][ceil].Item)
		}
	}

	return result
}

func (b Board) GetPieceWithColor(color string, pieceType int) (item *CheckmatePiece) {

	for i := 0; i <= 63; i++ {

		col := i % 8
		row := i / 8

		ceil := b.CeilList[row][col]
		if ceil.IsOccupied && ceil.Item.GetType() == pieceType && ceil.Item.GetColor() == color {
			item = &ceil.Item
			break
		}
	}

	return
}

func (b Board) GetPossibleMoves(color string) (result []controls.Move) {

	wg := sync.WaitGroup{}
	lock := sync.Mutex{}

	piecesList := b.GetPiecesForColor(color)
	for i := range piecesList {
		item := piecesList[i]

		wg.Add(1)

		go func() {
			moves, err := item.PossibleMoves(&b)
			if err != nil {
				panic(err)
			}

			lock.Lock()
			for i := range moves {
				result = append(result, moves[i])
			}
			lock.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()

	return
}

func (b Board) CheckMoveIsLegal(code, color string) (isLegal bool, err error) {
	move, err := controls.NewMoveFromString(code)
	if err != nil {
		return false, err
	}
	ceil, err := b.GetCeil(move.From)
	if err != nil {
		return false, err
	}
	if ceil.IsOccupied && ceil.Item.GetColor() == color {
		movesList, err := ceil.Item.PossibleMoves(&b)
		if err != nil {
			return false, err
		}

		for i := range movesList {
			if *move == movesList[i] {
				return true, nil
			}
		}
	}
	return false, err
}

func (b *Board) MakeMove(code string, gameState string) (err error) {
	move, err := controls.NewMoveFromString(code)
	if err != nil {
		return
	}

	ceil, err := b.GetCeil(move.From)
	if err != nil {
		return
	}

	if !ceil.IsOccupied {
		return fmt.Errorf("illigal move: %s", code)
	}

	err = b.UpdateCeil(false, nil, ceil.Item.GetPosition())
	if err != nil {
		return
	}

	err = b.UpdateCeil(true, ceil.Item, move.To)
	if err != nil {
		return
	}

	err = ceil.Item.Move(move.To)
	if err != nil {
		return
	}

	return
}

func (b Board) CheckIsBoardLimitationErrors(err error) bool {
	if err != nil && (err.Error() == LessThanZero ||
		err.Error() == XGreaterThanCeilLen ||
		err.Error() == YGreaterThanCeilLen) {
		return true
	}
	return false
}

func (b Board) CalculateDiagonal(pieceItem CheckmatePiece, limit int) (moves []controls.Move, err error) {

	for _, item := range [][]int{{1, 1}, {1, -1}, {-1, -1}, {-1, 1}} {
		calculated, err := calculate(item, pieceItem, &b, limit)
		if err != nil {
			return moves, err
		}

		for i := range calculated {
			moves = append(moves, calculated[i])
		}
	}
	return
}

func (b Board) CalculateStraight(pieceItem CheckmatePiece, limit int) (moves []controls.Move, err error) {

	// straight, right, down, left
	for _, item := range [][]int{{1, 0}, {-1, 0}, {0, 1}, {0, -1}} {
		calculated, err := calculate(item, pieceItem, &b, limit)
		if err != nil {
			return moves, err
		}

		for i := range calculated {
			moves = append(moves, calculated[i])
		}
	}
	return
}

func calculate(item []int, pieceItem CheckmatePiece, b *Board, limit int) (moves []controls.Move, err error) {

	currentPosition := pieceItem.GetPosition()

	max := 7
	if limit > 0 {
		max = limit
	}

	for i := 0; i < max; i++ {

		nextPosition := controls.NewPosition(currentPosition.Row+item[0], currentPosition.Col+item[1])

		nextCeil, err := b.GetCeil(nextPosition)

		if err != nil {
			if !b.CheckIsBoardLimitationErrors(err) {
				panic(err)
			} else {
				// All is ok, border limit occurred, so just stop
				break
			}
		}

		if nextCeil.IsOccupied {
			//can attack?

			if nextCeil.Item.GetColor() != pieceItem.GetColor() {
				move := controls.NewMove(
					controls.NewPosition(pieceItem.GetPosition().Row, pieceItem.GetPosition().Col),
					controls.NewPosition(nextPosition.Row, nextPosition.Col))

				moves = append(moves, *move)
			}

			break
		} else {
			move := controls.NewMove(
				controls.NewPosition(pieceItem.GetPosition().Row, pieceItem.GetPosition().Col),
				controls.NewPosition(nextPosition.Row, nextPosition.Col))

			moves = append(moves, *move)
			currentPosition = nextPosition
		}
	}
	return
}

func getBoardItems(blackOnTheTop bool, figures []string) (boardItems []CheckmatePiece) {

	white, black := 0, 7
	if !blackOnTheTop {
		white, black = 7, 0
	}

	for i := range figures {
		switch figures[i] {
		case "Rook":
			boardItems = append(boardItems, NewRook(white, 0, "white"))
			boardItems = append(boardItems, NewRook(black, 0, "black"))

			boardItems = append(boardItems, NewRook(white, 7, "white"))
			boardItems = append(boardItems, NewRook(black, 7, "black"))
			break
		case "Bishop":
			boardItems = append(boardItems, NewBishop(white, 1, "white"))
			boardItems = append(boardItems, NewBishop(black, 1, "black"))

			boardItems = append(boardItems, NewBishop(white, 6, "white"))
			boardItems = append(boardItems, NewBishop(black, 6, "black"))
			break
		case "Knight":
			boardItems = append(boardItems, NewKnight(white, 2, "white"))
			boardItems = append(boardItems, NewKnight(black, 2, "black"))

			boardItems = append(boardItems, NewKnight(white, 5, "white"))
			boardItems = append(boardItems, NewKnight(black, 5, "black"))
			break
		case "Queen":
			boardItems = append(boardItems, NewQueen(white, 3, "white"))
			boardItems = append(boardItems, NewQueen(black, 3, "black"))
			break
		case "King":
			boardItems = append(boardItems, NewKing(white, 4, "white"))
			boardItems = append(boardItems, NewKing(black, 4, "black"))
			break
		case "Pawn":
			pWhite, pBlack := 1, 6
			if !blackOnTheTop {
				pWhite, pBlack = 6, 1
			}
			for _, i := range []int{0, 1, 2, 3, 4, 5, 6, 7} {
				boardItems = append(boardItems, NewPawn(pWhite, i, "white"))
				boardItems = append(boardItems, NewPawn(pBlack, i, "black"))
			}
			break
		}
	}

	return boardItems
}

func getAllPieces() []string {
	return []string{"Queen", "Knight", "Bishop", "Rook", "King", "Pawn"}
}