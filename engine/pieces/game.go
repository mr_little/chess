package pieces

import (
	"chess/engine/controls"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
)

const (
	GameNotFound = "game with id '%s' not found"
	CHECK          = "CHECK"
	MATE           = "MATE"
)

type Game struct {
	Id           string             `json:"id"`
	Board        *Board             `json:"-"`
	Players      []*controls.Player `json:"-"`
	State        string             `json:"state"`
	currentColor string
}

func (g *Game) AddPlayer(p *controls.Player) (err error) {
	if len(g.Players) < 2 {
		g.Players = append(g.Players, p)

	} else {
		err = fmt.Errorf("limit of players exceed")
	}
	return
}

func (g Game) isCheck(color, kingColor string) (result bool, err error) {

	king := g.Board.GetPieceWithColor(kingColor, KING)

	if king != nil {
		possibleMoves := g.Board.GetPossibleMoves(color)

		for i := range possibleMoves {
			// if someone after the move have possibility to capture the opponent king, than this is a check
			if possibleMoves[i].To == (*king).GetPosition() {
				result = true
				break
			}
		}
	} else {
		err = fmt.Errorf("no %s king found", kingColor)
	}
	return
}

func (g Game) isMate(kingColor string) (result bool, err error) {

	king := g.Board.GetPieceWithColor(kingColor, KING)
	if king != nil {
		possibleMoves, err := (*king).PossibleMoves(g.Board)
		if err != nil {
			return result, err
		}
		if len(possibleMoves) == 0 {
			result = true
		}
	} else {
		err = fmt.Errorf("no %s king found", kingColor)
	}
	return
}

func (g *Game) Move(code string) error {

	if notLegal, err := g.Board.CheckMoveIsLegal(code, g.currentColor); err != nil || !notLegal {
		if !notLegal {
			return fmt.Errorf("move not legal")
		}
		return err
	}

	err := g.Board.MakeMove(code, g.State)
	if err != nil {
		return err
	}

	kingColor := "white"
	if g.currentColor == "white" {
		kingColor = "black"
	}

	check, err := g.isCheck(g.currentColor, kingColor)
	if err != nil {
		return err
	}

	if check {
		g.State = CHECK

		mate, err := g.isMate(kingColor)
		if err != nil {
			return err
		}
		if mate {
			g.State = MATE
		}
	}

	g.currentColor = kingColor

	return nil
}

func (g Game) GetCurrentColor() string {
	return g.currentColor
}

func NewGame(b *Board) *Game {
	g := &Game{Board: b}
	g.currentColor = "white"
	id, _ := uuid.NewV4()
	g.Id = id.String()
	return g
}

type GamesList struct {
	Items []*Game
}

func (g *GamesList) AddItem(item *Game) {
	g.Items = append(g.Items, item)
}

func (g GamesList) GetItems() []*Game {
	return g.Items
}

func (g GamesList) GetGameById(id string) (game *Game, err error) {
	for i := range g.Items {
		if g.Items[i].Id == id {
			game = g.Items[i]
			break
		}
	}

	if game == nil {
		return nil, fmt.Errorf(GameNotFound, id)
	}

	return
}

func NewGamesList() gin.HandlerFunc {

	gamesList := &GamesList{}

	return func(c *gin.Context) {
		c.Set("GAMES_LIST", gamesList)
		c.Next()
	}
}
