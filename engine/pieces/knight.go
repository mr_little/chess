package pieces

import (
	"chess/engine/controls"
)

func NewKnight(row, col int, color string) *Knight {
	k := Knight{}
	k.Color = color
	k.CurrentPosition.Row = row
	k.CurrentPosition.Col = col
	k.Name = "k"
	k.Type = KNIGHT
	return &k
}

type Knight struct {
	Piece
}

func (k Knight) PossibleMoves(b *Board) (moves []controls.Move, err error) {

	for _, item := range [][]int{
		{2, 1},
		{2, -1},
		{-1, 2},
		{1, 2},
		{-2, 1},
		{-2, -1},
		{1, -2},
		{-1, -2},
	} {
		nextPosition := controls.NewPosition(k.CurrentPosition.Row+item[0], k.CurrentPosition.Col+item[1])
		nextCeil, err := b.GetCeil(nextPosition)

		if err != nil {
			if !b.CheckIsBoardLimitationErrors(err) {
				panic(err)
			}
		} else {

			if !nextCeil.IsOccupied || nextCeil.Item.GetColor() != k.Color {
				move := controls.NewMove(
					controls.Position{Row: k.CurrentPosition.Row, Col: k.CurrentPosition.Col},
					controls.Position{Row: nextPosition.Row, Col: nextPosition.Col})
				moves = append(moves, *move)
			}
		}
	}

	return
}
