package pieces

import (
	"chess/engine/controls"
)

func NewBishop(row, col int, color string) *Bishop {
	b := Bishop{}
	b.Color = color
	b.CurrentPosition.Row = row
	b.CurrentPosition.Col = col
	b.Name = "b"
	b.Type = BISHOP
	return &b
}

type Bishop struct {
	Piece
}

func (b Bishop) PossibleMoves(board *Board) (moves []controls.Move, err error) {

	moves, err = board.CalculateDiagonal(&b, -1)

	return
}
