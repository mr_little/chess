package pieces

import (
	"chess/engine/controls"
)

func NewPawn(row, col int, color string) *Pawn {
	p := Pawn{}
	p.Color = color
	p.CurrentPosition.SetRow(row)
	p.CurrentPosition.SetCol(col)
	p.Name = "p"
	p.Type = PAWN
	return &p
}

type Pawn struct {
	Piece
	InitialStepMade bool
}

func (p *Pawn) Move(moveTo controls.Position) (err error) {

	err = p.Piece.Move(moveTo)
	if err != nil {
		return nil
	}

	if !p.InitialStepMade {
		p.InitialStepMade = true
	}

	return
}

func (p Pawn) PossibleMoves(b *Board) (moves []controls.Move, err error) {

	var invertMove bool
	if (p.Color == "black" && b.BlackOnTheTop) || (p.Color == "white" && !b.BlackOnTheTop) {
		invertMove = true
	}

	for _, item := range [][]int{{1, 0}, {2, 0}, {1, 1}, {1, -1}} {

		row, col := item[0], item[1]

		// If initial step for pawn already made, so no possibilities to make 2 step move again
		if row == 2 && p.InitialStepMade {
			continue
		}

		if invertMove {
			row *= -1
			col *= -1
		}

		nextPosition := controls.NewPosition(p.CurrentPosition.GetRow()+row, p.CurrentPosition.GetCol()+col)
		nextCeil, err := b.GetCeil(nextPosition)

		if err != nil {
			if !b.CheckIsBoardLimitationErrors(err) {
				return moves, err
			} else {
				continue
			}
		}

		// If cell is not occupied, all is ok. Otherwise check diagonal cell. If occupied and with other color figure, let's attack it
		if (!nextCeil.IsOccupied && col == 0) || (nextCeil.IsOccupied && (col == 1 && nextCeil.Item.GetColor() != p.Color)) {
			move := controls.NewMove(
				controls.NewPosition(p.CurrentPosition.GetRow(), p.CurrentPosition.GetCol()),
				controls.NewPosition(nextPosition.Row, nextPosition.Col))
			moves = append(moves, *move)
		}
	}

	return
}
