package pieces

import (
	"chess/engine/controls"
)

func NewKing(row, col int, color string) *King {
	k := King{}
	k.Color = color
	k.CurrentPosition.Row = row
	k.CurrentPosition.Col = col
	k.Name = "k"
	k.Type = KING
	return &k
}

type King struct {
	Piece
}

func (q King) PossibleMoves(board *Board) (moves []controls.Move, err error) {

	m, err := board.CalculateDiagonal(&q, 1)
	if err != nil {
		return
	}
	for i := range m {
		moves = append(moves, m[i])
	}

	m, err = board.CalculateStraight(&q,  1)
	if err != nil {
		return
	}
	for i := range m {
		moves = append(moves, m[i])
	}

	return
}
