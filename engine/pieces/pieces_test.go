package pieces

import (
	"os"
	"reflect"
	"sort"
	"testing"

	"chess/engine/controls"

	"github.com/stretchr/testify/assert"
)

var (
	board *Board
	game *Game
)

func init() {
	board = NewBoard()
	board.Init(true)

	game = NewGame(board)
}

func TestMain(m *testing.M) {

	retCode := m.Run()

	os.Exit(retCode)
}

func TestBishop_PossibleMoves(t *testing.T) {

	tests := []struct {
		name string
		call func() (interface{}, error)
		want []string
		wantError bool
		errString string
	} {
		{
			name: "Bishop move is ok",
			want : []string{"b1c2", "b1d3", "b1e4", "b1f5", "b1g6", "b1h7"},
			call: func() (interface{}, error) {
				err := game.Move("c2c4")
				if err != nil {
					return []string{}, err
				}

				err = game.Move("a7a6")
				if err != nil {
					return []string{}, err
				}

				//b1 is initial bishop position
				move, err := controls.NewMoveFromString("b1c2")
				if err != nil {
					return []string{}, err
				}

				ceil, err := board.GetCeil(move.From)
				if err != nil {
					return []string{}, err
				}

				moves, err := ceil.Item.PossibleMoves(board)
				if err != nil {
					return []string{}, err
				}

				got := make([]string, 0)
				for i := range moves {
					got = append(got, moves[i].String())
				}

				sort.Strings(got)

				return got, nil
			},
		},
		{
			name: "Bishop move is not legal",
			wantError: true,
			errString: "move not legal",
			call: func() (interface{}, error) {
				err := game.Move("b1e2")
				if err != nil {
					return []string{}, err
				}
				return []string{}, nil
			},
		},
		{
			name: "Knight move is ok",
			want : []string{"c1b3", "c1d3", "c1e2"},
			call: func() (interface{}, error) {
				err := game.Move("e2e4")
				if err != nil {
					return []string{}, err
				}

				err = game.Move("b7b6")
				if err != nil {
					return []string{}, err
				}

				//c1 is initial knight position
				move, err := controls.NewMoveFromString("c1e2")
				if err != nil {
					return []string{}, err
				}

				ceil, err := board.GetCeil(move.From)
				if err != nil {
					return []string{}, err
				}

				moves, err := ceil.Item.PossibleMoves(board)

				if err != nil {
					return []string{}, err
				}

				got := make([]string, 0)
				for i := range moves {
					got = append(got, moves[i].String())
				}

				sort.Strings(got)

				return got, nil
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got, err := tc.call()

			if tc.wantError {
				assert.NotNil(t, err)
				assert.EqualError(t, err, tc.errString)
			} else {
				assert.NoError(t, err)
				assert.True(t, reflect.DeepEqual(got, tc.want))
			}
		})
	}
}

