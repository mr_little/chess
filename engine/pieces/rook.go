package pieces

import (
	"chess/engine/controls"
)

func NewRook(row, col int, color string) *Rook {
	r := Rook{}
	r.Color = color
	r.CurrentPosition.Row = row
	r.CurrentPosition.Col = col
	r.Name = "r"
	r.Type = ROOK
	return &r
}

type Rook struct {
	Piece
}

func (r Rook) PossibleMoves(b *Board) (moves []controls.Move, err error) {

	moves, err = b.CalculateStraight(&r, -1)

	return
}
