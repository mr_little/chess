Chess Game
-----------

Game can be run as console application, or as web-server and used via api. 
For console use "-console" as run flag. 
Default port for web-server is 8090, if you want use another port, use flag "-port=PORT".
Console application has a simple visualization of game process.

Future improvements:
* Add more tests for pieces.
* Add castling for a king
* Add config
* Add UI

API:
-----
POST /games/ - Create new game

GET /games/ - Get list of games available

GET /games/:game_id/ - Get concrete game info

GET /games/:game_id/moves - Get list of legal moves for current color

POST /games/:game_id/moves?move=MOVE_CODE - Make a move for current color

 
GET /games/:game_id/players - Get list of current game players

POST /games/:game_id/players - Add player to the game
 
Simple example:
----------
curl -X POST http://HOST:PORT/games/ 
curl -X GET http://HOST:PORT/games/

curl -X GET http://HOST:PORT/games/GAME_ID/  
curl -X POST http://HOST:PORT/games/GAME_ID/players //Add first player to game
curl -X POST http://HOST:PORT/games/GAME_ID/players //Add second player to game

curl -X GET http://HOST:PORT/games/GAME_ID/moves 
curl -X POST http://HOST:PORT/games/GAME_ID/moves?move=e2e4  
 


