package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"

	"github.com/gin-gonic/gin"

	"chess/engine/pieces"
	"chess/web/routers"
	"chess/web/templates"
)

func runAsServer(portString string) {

	router := gin.New()
	router.Use(gin.Logger())
	router.Use(pieces.NewGamesList())

	router.HTMLRender = templates.LoadTemplates("./web/templates/html")

	routers.InitRouters(router)

	router.Use(gin.Recovery())

	panic(router.Run(":" + portString))
}

func runAsConsole() {
	board := pieces.NewBoard()
	board.Init(true)

	game := pieces.NewGame(board)
	scanner := bufio.NewScanner(os.Stdin)
	var move string

	// clear screen
	fmt.Println("\033[2J")
	fmt.Println(game.Board.Draw())
	for move != "q" {
		fmt.Println("\u001b[32mCurrent color:\u001b[31m", game.GetCurrentColor(), "\u001b[0m")
		fmt.Println("\u001b[32mPossible moves:\u001b[37m", game.Board.GetPossibleMoves(game.GetCurrentColor()), "\u001b[0m")
		fmt.Print("Next move(or q for quit) : ")
		scanner.Scan()
		move = scanner.Text()

		err := game.Move(move)
		if err != nil {
			fmt.Println(err, move)
		} else {
			fmt.Println("\033[2J")
			fmt.Println(game.Board.Draw())
		}
	}
}

func main() {
	asConsole := flag.Bool("console", false, "bool")
	serverPort := flag.String("port", "8090", "")

	flag.Parse()

	if *asConsole {
		runAsConsole()
	} else {
		runAsServer(*serverPort)
	}

}
