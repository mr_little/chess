package controllers

import (
	"chess/engine/pieces"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"chess/engine/controls"
)

type ERROR struct {
	Error string `json:"error"`
}

func possibleMovesAsList(legalMoves []controls.Move) (result map[string][]string) {
	result = make(map[string][]string)

	for i := range legalMoves {
		moveString := legalMoves[i].String()

		splitted := [2]string{}

		splitted[0] = string(moveString[:2])
		splitted[1] = string(moveString[2:])

		if _, ok := result[splitted[0]]; !ok {
			result[splitted[0]] = []string{}
		}

		result[splitted[0]] = append(result[splitted[0]], splitted[1])
	}

	return result
}

func MakeMove(c *gin.Context) {

	move := c.Query("move")
	if move == "" {
		c.JSON(http.StatusOK, ERROR{Error: fmt.Sprint("param 'move' is required")})
		return
	}

	gamesList := c.Keys["GAMES_LIST"].(*pieces.GamesList)
	gameId := c.Param("game_id")
	game, err := gamesList.GetGameById(gameId)

	if err != nil {
		c.JSON(http.StatusOK, ERROR{Error: err.Error()})
		return
	}

	if len(game.Players) < 2 {
		c.JSON(http.StatusOK, ERROR{Error: "not enough players"})
		return
	}

	err = game.Move(move)
	if err != nil {
		c.JSON(http.StatusOK, ERROR{Error: err.Error()})
		return
	}

	c.JSON(http.StatusOK, struct {
		State string `json:"state"`
	}{State: game.State})
}

func GetPossibleMoves(c *gin.Context) {
	gamesList := c.Keys["GAMES_LIST"].(*pieces.GamesList)

	gameId := c.Param("game_id")
	game, err := gamesList.GetGameById(gameId)

	if err != nil {
		c.JSON(http.StatusOK, ERROR{Error: err.Error()})
		return
	}

	if len(game.Players) < 2 {
		c.JSON(http.StatusOK, ERROR{Error: "not enough players"})
		return
	}

	moves := possibleMovesAsList(game.Board.GetPossibleMoves(game.GetCurrentColor()))

	c.JSON(http.StatusOK, struct {
		Moves        map[string][]string `json:"moves"`
		CurrentColor string              `json:"current_color"`
	}{Moves: moves, CurrentColor: game.GetCurrentColor()})

}
