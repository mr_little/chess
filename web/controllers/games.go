package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"chess/engine/pieces"
)

func GameTemplateView(c *gin.Context) {
	c.HTML(http.StatusOK, "game.html", gin.H{})
}

func CreateGame(c *gin.Context) {

	games := c.Keys["GAMES_LIST"].(*pieces.GamesList)

	board := pieces.NewBoard()
	board.Init(true)

	game := pieces.NewGame(board)
	games.AddItem(game)

	c.JSON(http.StatusOK, game)
}

func GetActiveGames(c *gin.Context) {

	gameId := c.Param("game_id")

	gamesList := c.Keys["GAMES_LIST"].(*pieces.GamesList)
	games := gamesList.GetItems()

	result := make([]pieces.Game, 0)
	for i := range games {
		if games[i].State == "MATE" {
			continue
		}

		if gameId != "" {
			if games[i].Id == gameId {
				result = append(result, *games[i])
				break
			}
		} else {
			result = append(result, *games[i])
		}
	}

	if gameId != "" {
		if len(result) == 0 {

			c.JSON(http.StatusOK, ERROR{Error: fmt.Sprintf(pieces.GameNotFound, gameId)})

		} else {
			c.JSON(http.StatusOK, result[0])
		}

	} else {
		c.JSON(http.StatusOK, result)
	}
}
