package controllers

import (
	"chess/engine/pieces"
	"net/http"

	"github.com/gin-gonic/gin"

	"chess/engine/controls"
)

func GetPlayers(c *gin.Context) {

	games := c.Keys["GAMES_LIST"].(*pieces.GamesList)

	gameId := c.Param("game_id")

	game, err := games.GetGameById(gameId)
	if err != nil {
		c.JSON(http.StatusOK, ERROR{Error: err.Error()})
	} else {
		c.JSON(http.StatusOK, game.Players)
	}
}

func AddPlayerToGame(c *gin.Context) {

	games := c.Keys["GAMES_LIST"].(*pieces.GamesList)
	gameId := c.Param("game_id")

	game, err := games.GetGameById(gameId)
	if err != nil {
		c.JSON(http.StatusOK, ERROR{Error: err.Error()})
	}

	color := ""
	if len(game.Players) == 1 {
		if game.Players[0].Color == "white" {
			color = "black"
		} else {
			color = "white"
		}
	}

	err = game.AddPlayer(controls.NewPlayer(color))
	if err != nil {
		c.JSON(http.StatusOK, ERROR{Error: err.Error()})
	}

	c.JSON(http.StatusOK, game.Players)
}
