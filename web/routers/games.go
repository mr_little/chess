package routers

import (
	"github.com/gin-gonic/gin"

	"chess/web/controllers"
)

func InitGameRouter(router *gin.Engine) {

	router.GET("/game", controllers.GameTemplateView)

	moves := router.Group("/games")

	moves.GET("/", controllers.GetActiveGames)
	moves.POST("/", controllers.CreateGame)

	moves.GET("/:game_id/", controllers.GetActiveGames)
}
