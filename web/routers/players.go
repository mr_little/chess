package routers

import (
	"github.com/gin-gonic/gin"

	"chess/web/controllers"
)

func InitPlayersRouter(router *gin.Engine) {

	moves := router.Group("/games")

	moves.GET("/:game_id/players", controllers.GetPlayers)
	moves.POST("/:game_id/players", controllers.AddPlayerToGame)
}
