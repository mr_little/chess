package routers

import (
	"github.com/gin-gonic/gin"

	"chess/web/controllers"
)

func InitMovesRouter(router *gin.Engine) {

	moves := router.Group("/games")

	moves.GET("/:game_id/moves", controllers.GetPossibleMoves)
	moves.POST("/:game_id/moves", controllers.MakeMove)
}
