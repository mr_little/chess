package routers

import (
	"github.com/gin-gonic/gin"
)

func InitRouters(router *gin.Engine) {

	InitGameRouter(router)
	InitMovesRouter(router)
	InitPlayersRouter(router)
}
